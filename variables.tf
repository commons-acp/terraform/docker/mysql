variable "docker_mysql_image" {
  default = "mysql:5.7"
}

variable "container_mysql_name" {
  description = ""
}

variable "network_name" {
  description = "network name of the container"
}

variable "host_mysql_path" {
  description = ""
}

variable "mysql_env" {
  type = set(string)
  description = ""
}

variable "external_port"{
  default = 3306
}

variable "container_path" {
  description = "path of the container's inner volume"
  default = "/var/lib/mysql"
}
