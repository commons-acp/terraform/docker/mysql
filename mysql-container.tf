resource "docker_image" "mysql_container_image" {
  name          = data.docker_registry_image.mysql.name
  pull_triggers = [data.docker_registry_image.mysql.sha256_digest]
  keep_locally  = true
}

resource "docker_network" "network_bridge" {
  name = var.network_name
  driver = "bridge"
}

resource "docker_container" "mysql_container" {
  depends_on =[docker_network.network_bridge]
  image = docker_image.mysql_container_image.latest
  name  = var.container_mysql_name

  volumes {
    host_path = var.host_mysql_path
    container_path = var.container_path
    read_only = false
  }

  ports {
    internal = 3306
    external = var.external_port
  }

  env = var.mysql_env


  networks_advanced {
    name = var.network_name
  }



}
