terraform {
  required_providers {
    docker = {
      "source": "terraform-providers/docker"
    }
  }
}
data "docker_registry_image" "mysql" {
  name = var.docker_mysql_image
}
